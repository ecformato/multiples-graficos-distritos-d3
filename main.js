const dataDistrict = 'https://docs.google.com/spreadsheets/d/e/2PACX-1vTd7_oWpmsbfDOFTJpOlR1rqD21cGfqm78tCDP4kGaTahnx0YrrGgAy8QoerC1iC-XiYNbjbXHna03W/pub?gid=1542721701&single=true&output=tsv';

//Mensaje a AMP para cambio de altura
// window.parent.postMessage({
//     sentinel: 'amp',
//     type: 'embed-size',
//     height: document.body.scrollHeight
// }, '*');

let urlParams = new URLSearchParams(window.location.search);
let typeParam1 = urlParams.get('ciudad');
let typeParam2 = urlParams.get('datos');

let margin = {top: 20, right: 15, bottom: 20, left: 40};
let width = 180, height = 120;

//Tooltip para gráfico
let tooltipDiv = d3.select('body').append('div')
    .style('background', '#a7c9c7')
    .style('top', '0px')
    .style('left', '0px')
    .style('border-radius', '2px')
    .style('padding', '5px')
    .style('position', 'absolute')
    .style("z-index", "2000")
    .style("display", "none")
    .style('font-size', '0.9rem')
    .style('color', '#000')
    .style('text-align','center');

d3.tsv(dataDistrict, (d) => {
    return {
        ciudad: d.Ciudad,
        distrito: d.Distrito,
        fecha: formatTime(d.Fecha),
        anuncios: +d.Anuncios,
        precio_medio: +d.Precio_Medio.replace(',','.')
    }
}).then((data) => {
    //Parámetros URL para mostrar unos datos u otros
    if(typeParam1 == 'madrid' && typeParam2 == 'anuncios'){
        createChart(data, 'madrid','anuncios');
    } else if (typeParam1 == 'madrid' && typeParam2 == 'precios'){
        createChart(data, 'madrid', 'precios');
    } else if (typeParam1 == 'barcelona' && typeParam2 == 'anuncios'){
        createChart(data, 'barcelona','anuncios');
    } else if (typeParam1 == 'barcelona' && typeParam2 == 'precios'){
        createChart(data, 'barcelona','precios');
    } else {
        createChart(data, 'madrid','anuncios');
    }

    function createChart(data, ciudad, tipoDato){
        let distritosFiltrados = data.filter((item) => {
            return item.ciudad.toLowerCase() == ciudad && item;
        });

        //Agrupamos por distritos
        let distritos = d3.nest()
            .key(function(d) {return d.distrito;})
            .entries(distritosFiltrados);

        let chart = d3.select("#chart")
            .selectAll('svg')
            .data(distritos)
            .enter()
            .append("svg")
            .attr('class','marcadorAncho')
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        //Eje X
        let x = d3.scaleTime()
            .range([0,width])
            .domain([new Date(2019,02,26), new Date(2020,05,15)]);

        let xAxis = svg => svg
            .call(d3.axisBottom(x).ticks(0))
            .call(g => g.selectAll('path').attr('stroke','#818181'));

        chart.append("g")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis);           

        //Segundo eje X
        let x_v2 = d3.scaleBand()
            .range([0,width])
            .domain([1,2]);
        
        let xAxis_v2 = svg => svg
            .call(d3.axisBottom().tickFormat((d) => {return d == 1 ? '03/19' : '06/20' }).scale(x_v2).ticks(1))
            .call(g => g.selectAll('.tick').attr('transform', (d) => {return d == 1 ? `translate(0,0)` : `translate(${document.getElementsByClassName('marcadorAncho')[0].getBoundingClientRect().width - 57.5},0)`}))
            .call(g => g.selectAll('.tick text').style('font-family','Roboto').style('font-weight','200').style('font-size','13px').style('fill','#818181').attr('text-anchor', (d) => { return d == 1 ? 'start' : 'end'}))
            .call(g => g.selectAll('.tick line').remove())
            .call(g => g.selectAll('path').remove());
        
        chart.append("g")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis_v2);

        //Eje Y
        let y = d3.scaleLinear()
        .range([height - 20,0])
        .domain([tipoDato == 'anuncios' ? 0 : 10,tipoDato == 'anuncios' ? 5000 : 25]);

        let yAxis = svg => svg
            .call(d3.axisLeft(y).ticks(3).tickFormat((d) => {return tipoDato == 'precios' ? getNumberWithCommas(d) + '€' : getNumberWithCommas(d)}))
            .call(g => g.selectAll('.tick text').style('font-family','Roboto').style('font-weight','200').style('font-size','13px').style('fill','#818181'))
            .call(g => g.selectAll('.tick text'))
            .call(g => g.selectAll('line').remove())
            .call(g => g.select('path').style('stroke','#818181'));

        chart.append('g')
            .attr('transform', `translate(0, 20)`)
            .call(yAxis);
        
        // chart.append('g')
        //     .selectAll('.prueba')
        //     .enter()
        //     .each((d,i) => {
        //     console.log(d);
        //         var min = d3.min(d.values, d => d)
        //         var max = d3.max(d.values, d => d)
                
        //         var y = d3.scaleLinear()
        //             .domain([max, min])
        //             .range([height, 0])
                
        //         var yAxis = d3.axisLeft(y)
        //             .ticks(3)

        //         d3.select(this)
        //             .call(yAxis)
        //     })

        chart
            .append("text")
            .attr("text-anchor", "start")
            .attr("y", 0)
            .attr("x", 0)
            .text(function(d){ return(d.key.toUpperCase())})
            .style('fill', '#a7c9c7')
            .style('font-size','15px')
            .style('font-weight','900')
            .style('font-family','Roboto');
        
        //Línea COVID
        chart.append("svg:line")
            .attr("stroke-width", "1px")
            .attr("stroke", "#818181")
            .attr("x1", x(new Date(2020,02,14)))
            .attr("y1", height)
            .attr("x2", x(new Date(2020,02,14)))
            .attr("y2", 20);

        //Lógica líneas
        let line = d3.line()
            .x((d) => { return x(d.fecha) })
            .y((d) => { return tipoDato == 'anuncios' ? y(d.anuncios) + 20 : y(d.precio_medio) + 20; });
        
        //Dibujo líneas
        chart.append("path")
        .attr('fill','none')
        .attr("stroke", "#ff6666")
        .attr("stroke-width", "1px")
        .attr("d", function(d){
            // var min = d3.min(d.values, d => d)
            // var max = d3.max(d.values, d => d)
            
            // var y = d3.scaleLinear()
            //     .domain([max, min])
            //     .range([height, 0]);
            
            // var line = d3.line()
            //     .x(function(d) { return x(d.fecha); })
            //     .y(function(d) { return y(+d.anuncios); })
            
            // return line(d.values);
            return line(d.values);
        });

        //Dibujo círculos
        chart.selectAll("circle")
        .data(function(d){return d.values})
        .enter()
        .append("circle")
        .attr("r", 3)
        .attr("cx", function(d) { return x(d.fecha) })
        .attr("cy", function(d) { return tipoDato == 'anuncios' ? y(d.anuncios) + 20 : y(d.precio_medio) + 20; })
        .style("fill", '#ff6666')
        .style("opacity","0")
        .on('mouseover', (d, i, f) => {
            let circulo = f[i];
            circulo.style.opacity = '1';

            let bodyWidth = parseInt(d3.select(`#chart`).style('width'));
            let mapHeight = parseInt(d3.select(`#chart`).style('height'));

            let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
            let downSide = mapHeight / 2 > d3.event.pageY ? false : true;

            tooltipDiv.transition()     
                .duration(200)
                .style('display', 'block')
                .style('width', (d) => {return tipoDato == 'anuncios' ? '40px' : '80px'})
            tooltipDiv.html(`<span style="font-weight: 900;">${tipoDato == 'anuncios' ? getNumberWithCommas(d.anuncios) : getNumberWithCommas(d.precio_medio)}</span><span>${tipoDato == 'anuncios' ? '' : ' €/m2'}</span>`)
                .style("left", (downSide && rigthSide ? d3.event.pageX - 50 : downSide && !rigthSide ? d3.event.pageX + 15 : !downSide && rigthSide ? d3.event.pageX - 50 : d3.event.pageX - 15) + "px")     
                .style("top", (downSide && rigthSide ? d3.event.pageY - 50 : downSide && !rigthSide ? d3.event.pageY - 50 : !downSide && rigthSide ? d3.event.pageY - 37.5 : d3.event.pageY - 32.5) + "px");
        })
        .on('mouseout', (d, i, f) => {
            let circulo = f[i];
            circulo.style.opacity = '0';
        });

        let charts = document.getElementsByClassName('marcadorAncho');
        for(let i = 0; i < charts.length; i++){
            charts[i].addEventListener('mouseleave', () => {getOutTooltip()});
        }
    }
    
}).catch((error) => {console.log(error);})

function getOutTooltip(){
    tooltipDiv.style('display','none');
}

function formatTime(date) {
    let split = date.split("/");
    return new Date(split[2], split[1] - 1, split[0]);
}

d3.timeFormatDefaultLocale({
    "decimal": ",",
    "thousands": ".",
    "grouping": [3],
    "currency": ["€", ""],
    "dateTime": "%a %b %e %X %Y",
    "date": "%d/%m/%Y",
    "time": "%H:%M:%S",
    "periods": ["AM", "PM"],
    "days": ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
    "shortDays": ["Dom", "Lun", "Mar", "Mi", "Jue", "Vie", "Sab"],
    "months": ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"],
    "shortMonths": ["ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"]
});

function getNumberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}